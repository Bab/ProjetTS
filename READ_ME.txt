This python project is divided in two files :
- "projet.py" contains all the functions hat can be used by an extern user
- "test_maker.py" uses the functions of "project.py" to compute different output files. Feel free to change and experiment with the parameters used.

Usage : in a terminal, type "python3 test_maker.py"

Given the input wav file "phrase.wav", this should produce several other wav files :
- "phrase2.wav" is a simple reconstruction of the original file
- "slow1.wav" is a naively slowed down version
- "slow2.wav" use STFT ans ISTFT with different time steps to get a slower signal but don't have any phase shifting
- "slow3.wav" is the same as "slow2.wav" except with phase shifting
- "slow4.wav" is accelerated linearly.