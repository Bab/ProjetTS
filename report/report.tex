\documentclass[a4paper,11pt]{article}

\usepackage{fullpage}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{tikz}
\usepackage{pgfplots}

\newcommand\plotscale{0.75}
\newcommand\linescale{2}

\usepackage[backend=biber]{biblatex}
\bibliography{Biblio}{}



\title{Projet TS : Découplage temps/fréquence}
\date{}
\author{Rémy Sun \and Bastien Thomas}

\begin{document}

\maketitle

\section{Présentation du problème}

Lorsqu'un son (\texttt{phrase.wav}) est ralenti ou accéléré, sa hauteur est aussi
modifiée (\texttt{slow1.wav}).
Cela est du au lien qui existe entre le temps et la fréquence: la fréquence est
ce qui donne à l'oreille humaine la hauteur d'un son.
Nous allons voir ici qu'il est toutefois possible de corriger ce phénomène,
permettant ainsi d'obtenir un son plus rapide ou plus lent sans modifier sa hauteur,
ou au contraire de modifier sa hauteur sans modifier sa fréquence.

\begin{figure}[!h]
   \centering
    \begin{minipage}{.5\textwidth}
        \centering
        \label{SonOriginal}
        \begin{tikzpicture}[scale = \plotscale]
           \begin{axis}[]
              \addplot[blue, domain = 0:2, samples=100] {sin(deg(2*pi*2*x))};
           \end{axis}%
        \end{tikzpicture}%
        \caption{Son original}
    \end{minipage}%
    \begin{minipage}{.5\textwidth}
        \centering
        \label{SonRalenti}
        \begin{tikzpicture}[scale = \plotscale]
           \begin{axis}[]
             \addplot[blue, domain = 0:2, samples=100] {sin(deg(2*pi*x))};
           \end{axis}%
        \end{tikzpicture}%
        \caption{Son ralenti}
    \end{minipage}
\end{figure}

Nous allons commencer par rappeler les définitions et propriétés de la transformée de Fourrier à court terme (STFT) ainsi que de son inverse.
Nous présenterons ensuite une première version de l'algorithme permettant de modifier la longueur d'un son, mais nous verrons que cette méthode présente encore des défauts.
Nous verrons enfin comment corriger certains de ces défauts, et discuterons d'autres améliorations possibles.

\section{Rappels : Transformée de Fourrier à Court Terme}

Lorsque l'on souhaite traiter un signal, on dispose de deux points de vue :
\begin{itemize}
    \item Le point de vue temporel, où on décompose le signal en valeur par pas de temps.
    \item Le point de vue fréquentiel, où on s'intéresse aux fréquences présentes dans le signal.
\end{itemize}

On peut passer d'une représentation à l'autre simplement par transformée de Fourrier et transformée inverse,
mais cela ne permet pas d'utiliser le domaine fréquentiel pour modifier
uniquement le
domaine temporel.
En effet, la transformée de Fourier donne des informations sur la représentation
fréquentielle de l'ensemble du signal. Hors ce qui nous intéresse sont en partie
des variations locales de ce signal, et le spectre associé.

La transformée de Fourrier à court terme (STFT) vise à corriger ce problème en fournissant à la fois des informations temporelles et fréquentielles.
Elle est construite de la manière suivante :
\begin{enumerate}
    \item On découpe le signal en fenêtres temporelle qui se chevauchent.
    \item On effectue une transformée de Fourrier sur chacune de ces fenêtres.
\end{enumerate}

Concrètement, on obtient cette formule :
$$ X(r, k) = \sum_{n = 0}^{N - 1} x[rT + n] w[n] e^{-2 \pi n k / N} $$
Où :
\begin{description}
\item[$r$] est le numéro de la fenêtre
\item[$k$] est l'indice de la fréquence
\item[$w$] est la fenêtre utilisée
\item[$T$] est le décalage entre les fenêtres (généralement inférieur au support de $w$)
\end{description}

\begin{figure}[!h]
\centering
\begin{tikzpicture}[scale = \linescale]
\def\T{0.55}
\draw (0, 1) -- (7 * \T + 1, 1) node[above] {$x[n]$};
\foreach \k in {0, 2, ..., 6} {
  \draw[color = red] (\k * \T, 0.8) -- (\k * \T + 1, 0.8);
}
\foreach \k in {1, 3, ..., 7} {
  \draw[color = red] (\k * \T, 0.6) -- (\k * \T + 1, 0.6);
}
\draw[color = red] (7 * \T + 1, 0.6) node[below] {$X(r, k)$};
\end{tikzpicture}
\caption{Illustration de la transformée de Fourrier à court terme}
\end{figure}

La STFT est indexée par deux indices :
\begin{itemize}
\item $r$ indique le numéro de la fenêtre, il permet de représenter l'évolution temporelle du signal.
\item $k$ permet une analyse fréquentiel du signal.
\end{itemize}
On peut alors voir la STFT comme un tableau à deux dimension, où le temps est représenté horizontalement et les fréquences verticalement.


On dispose aussi d'une méthode de reconstitution appelée Overlap-add :
\begin{enumerate}
\item IFFT sur chaque fenêtre
\item On additionne tous les signaux obtenus fenêtrés par une fenêtre de reconstitution
\end{enumerate}

Sous certaines hypothèses, on ré-obtient ainsi le signal d'origine. En
particulier, l'utilisation d'une fenêtre de Hanning en synthèse et d'une fenêtre
unitaire en reconstitution donne en théorie une reconstitution parfaite. Un
exemple d'une telle reconstitution est fournie par le fichier \texttt{phrase2.wav}


\section{Application de la STFT au problème}

Nous avons vus dans la partie précédente que la STFT permettait d'obtenir des informations à la fois temporelles et fréquentielles sur un signal.
Nous allons voir ici comment on peut en profiter pour modifier la longueur d'un signal sans en modifier la durée.

L'idée naturelle consiste à modifier le décalage entre les fenêtres lors de la reconstruction du signal (voir Figure \ref{reconstruction décalée}).

\begin{figure}[!h]
\centering
\begin{minipage}{\textwidth}
\centering
\begin{tikzpicture}[scale = \linescale]
\def\T{0.55}
\draw (0, 1) -- (7 * \T + 1, 1) node[above] {$x[n]$};
\foreach \k in {0, 2, ..., 6} {
  \draw[color = red] (\k * \T, 0.8) -- (\k * \T + 1, 0.8);
}
\foreach \k in {1, 3, ..., 7} {
  \draw[color = red] (\k * \T, 0.6) -- (\k * \T + 1, 0.6);
}
\draw[color = red] (7 * \T + 1, 0.6) node[below] {$X(r, k)$};
\end{tikzpicture}
\end{minipage}

\begin{minipage}{\textwidth}
\centering
\begin{tikzpicture}[scale = \linescale]
\def\T{0.8}
\draw (0, 1) -- (7 * \T + 1, 1) node[above] {$x[n]$};
\foreach \k in {0, 2, ..., 6} {
  \draw[color = red] (\k * \T, 0.8) -- (\k * \T + 1, 0.8);
}
\foreach \k in {1, 3, ..., 7} {
  \draw[color = red] (\k * \T, 0.6) -- (\k * \T + 1, 0.6);
}
\draw[color = red] (7 * \T + 1, 0.6) node[below] {$X(r, k)$};
\end{tikzpicture}
\end{minipage}
\caption{Illustration de la reconstruction décalée}
\label{reconstruction décalée}
\end{figure}

\section{Redécalage de la phase}

\subsection{C'est mieux, mais pas tant que ça!}

Cette version de l'algorithme fonctionne globalement.
Mais on entend tout de même (\texttt{slow2.wav}) une sorte de craquement désagréable lorsqu'elle est appliquée sur des sons.
Cela est du au fait que nous ne tenons pas compte de la phase lors du décalage des fenêtres, induisant ainsi des sauts de phase.

\begin{figure}[!h]
\centering
\begin{minipage}{.5\textwidth}
\centering
\begin{tikzpicture}[scale = \plotscale]
\begin{axis}[domain = 0:3]
\addplot[blue, domain = 0:2, samples=100] {sin(deg(2*pi*x)) + 0.8};
\addlegendentry{$X(r, \_)$}
\addplot[red, domain = 1:3, samples=100] {0.6 * sin(deg(2*pi*x)) - 0.4};
\addlegendentry{$X(r + 1, \_)$}
\end{axis}%
\end{tikzpicture}%
\caption{Avant décalage}%
\end{minipage}%
\begin{minipage}{.5\textwidth}
\centering
\begin{tikzpicture}[scale = \plotscale]
\begin{axis}[domain = 0:3.7]
\addplot[blue, domain = 0:2, samples=100] {sin(deg(2*pi*x)) + 0.8};
\addlegendentry{$X(r, \_)$}
\addplot[red, domain = 1.7:3.7, samples=100] {0.6 * sin(deg(2*pi*(x - 0.7))) - 0.4};
\addlegendentry{$X(r + 1, \_)$}
\end{axis}%
\end{tikzpicture}
\caption{Après décalage sans raccordement}
\end{minipage}
\end{figure}

\subsection{Synchronisation horizontale}

Pour corriger ce problème, il faut trouver quelle phase donner à chacune des fenêtres pour limiter ces décalages.

\begin{figure}[!h]
\centering
\begin{tikzpicture}[scale = 0.7]
\begin{axis}[domain = 0:3.7]
\addplot[blue, domain = 0:2, samples=100] {sin(deg(2*pi*x)) + 0.8};
\addlegendentry{$X(r, \_)$}
\addplot[red, domain = 1.7:3.7, samples=100] {0.6 * sin(deg(2*pi*x)) - 0.4};
\addlegendentry{$X(r + 1, \_)$}
\end{axis}%
\end{tikzpicture}
\caption{Après décalage avec raccordement}
\end{figure}

Pour cela, nous disposons de la formule suivante :
$$ \arg\left(Y(r+1, k)\right) = \arg\left(Y(r, k)\right) + 2 \pi I f_k $$

Où $I$ est un facteur dépendant du facteur d'agrandissement du signal et $f_k$
la fréquence principalement prise en compte par le coefficient de la TFCT
$X(r,k)$. En effet, Il s'agit simplement de considérer la modification de la
phase à l'origine entre les deux fenêtres. 

L'algorithme amélioré deviens alors :

\begin{enumerate}
    \item Calcul de la transformée de Fourier court terme avec décalage $T_a$
    \item Calcul de la phase ajustée à un décalage $T_s$ pour chaque fenêtre
    \item Reconstruction avec décalage $T_s$
\end{enumerate}

Les résultats obtenus par cette méthodes sont globalement meilleurs, bien qu'il
reste encore quelques défauts.

Une transformation pour un ralentissement constant \texttt{slow3.wav} et
linéairement accéléré \texttt{slow4.wav} sont fournis

\subsection{Synchronisation verticale}

Il demeure un problème à l'oreille quand on écoute le signal ainsi reconstitué.
En effet, nous avons effectué une synchronisation de la phase pour chaque
fréquence \textbf{de fenêtre en fenêtre}. néanmoins, rien ne garantit qu'il n'existe pas
plusieurs fréquences proches qui ont été modifiées pour un raccordement de
phase, ou même que la modification de phase au sein d'une fenêtre soit cohérente
de fréquence en fréquence.

Ainsi, il faut effectuer un raccordement de phase de \textbf{fréquence en
  fréquence}. Des méthodes à cet effet ont été proposées par
\cite{laroche1999improved} et \cite{puckette1995phase}

\section{Changement de tonalité}

\paragraph{Et si c'est justement la tonalité du signal que l'on veut modifier?}

Nous avons montré deux grand types de méthodes de changement de durée: celles
altérant la tonalité et celles la préservant. Dès lors, il s'agit juste de
déterminer quelle transformation du premier type permet d'obtenir la tonalité
voulue et l'effectuer. Il suffit par la suite de compenser par une
transformation du second type pour revenir à la durée originale.

Un exemple de cette méthode avec raccordement horizontal de phase est donné
(\texttt{high.wav}). Le résultat souffre des mêmes problèmes que le signal de
durée modifiée, mais cela reste mieux qu'une approche naïve.

\paragraph{Pourquoi faire ça?}

On peut par exemple générer un synthétiseur à partir d'un seul son d'une note de
référence (Le diapason donne littéralement le ton!). De manière plus
pragmatique, cela peut permettre une transformation de message qui rend plus
difficile l'identification du locuteur sans pour autant changer la
compréhensibilité du message. Industriellement, une telle méthode peut-être
utilisée pour corriger les lignes de chant sur des albums musicaux (auto-tune!)


\section{Conclusion}

Au cours de projet, nous nous sommes intéressés au problème du découplage temps/fréquence d'un signal.
Nous avons ainsi pu accélérer ou ralentir un son, mais aussi en modifier la hauteur, et cela même de manière non uniforme.

Il est encore possible d'améliorer ce système, par exemple en implémentant la synchronisation verticale de la phase.

\appendix

\printbibliography

\end{document}