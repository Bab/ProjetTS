import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import scipy.signal as sig
from scipy.io.wavfile import read as wavread
from scipy.io.wavfile import write as wavwrite

def stft(x, fsize, T):
  ''' Compute the Short Time Fourrier Transform of the input signal x,
  with a Hanning window of size fsize, and a time step of T '''
  w = sp.hanning(fsize)
  return sp.array([sp.fft(w * x[i: i+fsize]) for i in range(0, len(x) - fsize, T)])

def istft(X, T):
  ''' Recompute the initial signal from a STFT X with a time step of T '''
  nf, fsize = X.shape
  nsig = nf * T + fsize
  x = sp.zeros(nsig)
  for i, Y in enumerate(X):
    x[i*T: i*T+fsize] += sp.real(sp.ifft(Y))
  return x

def normalize(z):
  ''' Normalize a complex between 0 and 1 '''
  return z/abs(z)
 
def principal(dphi):
  ''' Compute the principal phase (betwwen -pi and +pi) of an input phase '''
  while dphi < -np.pi: 
    dphi += 2*np.pi
  while dphi >= np.pi: 
    dphi -= 2*np.pi
  return dphi


def h_rephase(X, Ti, Tf):
  ''' Shift the phase of a STFT X computed with time step Ti so that it
  can be properly recomputed with time step Tf'''
  n, fsize = X.shape
  r = sp.zeros([n, fsize], dtype = X.dtype)
  omega = np.array([2*np.pi*k/fsize for k in range(fsize)])                
  for k, xf in enumerate(X.T): # iter on frequences
    r[0, k] = xf[0]
    for i in range(1, n):
      # principal component of heterodyne phase
      het = principal((np.angle(xf[i]) - np.angle(xf[i-1]) - omega[k]*Ti))
      # instantatneous frequency
      dphi = omega[k] + (1/Ti)*het
      # Modified phase
      phi = (np.angle(r[i-1,k]) + Tf * dphi)
      r[i, k] = abs(xf[i]) * np.exp(phi*1j)
  return r

def h_rephase_acc(X, Ti, Tf, inc):
  ''' Same as h_rephase, but with a linear increment on Tf '''
  n, fsize = X.shape
  Tw = Tf
  r = sp.zeros([n, fsize], dtype = X.dtype)
  omega = np.array([2*np.pi*k/fsize for k in range(fsize)])                
  for k, xf in enumerate(X.T): # iter on frequences
    if k % 50 == 0:        
      Tw = Tw - inc
      r[0, k] = xf[0]
    for i in range(1, n):
      # principal component of heterodyne phase
      het = principal((np.angle(xf[i]) - np.angle(xf[i-1]) - omega[k]*Ti))
      # instantatneous frequency
      dphi = omega[k] + (1/Ti)*het
      # Modified phase
      phi = (np.angle(r[i-1,k]) + Tw * dphi)
      r[i, k] = abs(xf[i]) * np.exp(phi*1j)
  return r
    
def istft_acc(X, T, inc):
  ''' Compute an ISTFT with a non-constant time step '''
  nf, fsize = X.shape
  nsig = nf * T + fsize
  x = sp.zeros(nsig)
  Tw = T
  pos = 0
  for i, Y in enumerate(X):
    x[pos: pos+fsize] += sp.real(sp.ifft(Y))
    pos += Tw
    if i % 50 == 0:
      Tw -= inc
  return x
