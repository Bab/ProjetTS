# -*- coding: utf-8 -*-
"""
Created on Thu Dec  1 16:23:48 2016

@author: remy
"""

import numpy as np
import matplotlib.pyplot as plt
import math
import cmath
import scipy
from numpy.fft import fft
import scipy.signal as sig
from scipy.io.wavfile import read as wavread
from scipy.io.wavfile import write as wavwrite
import scipy.io.wavfile
from pylab import *
import sys

plt.close('all')

#%% methode naïve

phrase = open('phrase.wav','rb')
p = wavread(phrase)
phrase.close()

l = [x for pair in zip(p[1],p[1]) for x in pair]

f = open('fast.wav','wb')
wavwrite(f,p[0],array(l,dtype='int16'))
f.close()

#%% TFCT

def stft(x, fs, framesz, hop):
    """
     x - signal
     fs - sample rate
     framesz - frame size
     hop - hop size (frame size = overlap + hop size)
    """
    framesamp = int(framesz*fs)
    hopsamp = int(hop*fs)
    w = scipy.hanning(framesamp)
    X = scipy.array([scipy.fft(w*x[i:i+framesamp]) 
                     for i in range(0, len(x)-framesamp, hopsamp)])
    return X

def istft(X, fs, T, hop):
    """ T - signal length """
    length = T*fs
    x = scipy.zeros(T*fs)
    framesamp = X.shape[1]
    hopsamp = int(hop*fs)
    for n,i in enumerate(range(0, len(x)-framesamp, hopsamp)):
        x[i:i+framesamp] += scipy.real(scipy.ifft(X[n]))
    # calculate the inverse envelope to scale results at the ends.

    return x # right side is still a little messed up..

#%% Verify stft reconstitution
    
phrase = open('phrase.wav','rb')
p = wavread(phrase)
phrase.close()

amp = max(p[1])

i = stft(p[1],8000,0.05,0.025)
r = istft(i,8000,3.9,0.05)

figure(3)
plt.plot(r)

f = open('fast.wav','wb')
wavwrite(f,p[0],np.array(amp*r/max(r),dtype='int16'))
f.close()

#%% stft rescaling

N = 512
H = N/2

tscale = 1.5

# read input and get the timescale factor
(sr,signalin) = wavread('phrase.wav')
L = len(signalin)
tscale = float(tscale)

# signal blocks for processing and output
sigout = zeros(L*tscale+N)

# max input amp, window
amp = max(signalin)
win = scipy.hanning(N)
p = 0
pp = 0

while p < L-(N+H):

    # take the spectra of two consecutive windows
    p1 = int(p)
    spec =  fft(win*signalin[p1:p1+N])

    sigout[pp:pp+N] += (ifft((spec))).real
    pp += H*tscale
    p += H

figure(1)
plt.plot(sigout)


f = open('resc.wav','wb')
wavwrite(f,sr,array(amp*sigout/max(sigout), dtype='int16'))
f.close()

#%% Vocoder horizontal

N = 512
H = N/2

tscale = 1.5
# read input and get the timescale factor
(sr,signalin) = wavread('phrase.wav')
L = len(signalin)
tscale = float(tscale)

# signal blocks for processing and output
phi  = zeros(N)
dphi  = zeros(N)
out = zeros(N, dtype=complex)
sigout = zeros(L*tscale+N)

# max input amp, window
amp = max(signalin)
win = scipy.hanning(N)
p = 0
pp = 0

while p < L-(N+H):

    # take the spectra of two consecutive windows
    p1 = int(p)
    
    spec1 =  fft(win*signalin[p1:p1+N])
    spec2 =  fft(win*signalin[p1+H:p1+N+H])
    M=len(spec1)
    omega = array([2*pi*k/M for k in range(M)])
    # take their phase difference and integrate
    dphi = (np.angle(spec2) - np.angle(spec1) - omega*H)
    # bring the phase back to between pi and -pi
    for i in dphi:
        while i < -pi: i += 2*pi
        while i >= pi: i -= 2*pi
    dphi = dphi/H+omega
    phi += dphi*H*tscale
    for i in phi:
        while i < -pi: i += 2*pi
        while i >= pi: i -= 2*pi
    out.real, out.imag = cos(phi), sin(phi)
    # inverse FFT and overlap-add
    sigout[pp:pp+N] += (ifft((spec2)*out)).real
    pp += H*tscale
    p += H

figure(2)
plt.plot(sigout)
plt.show()

f = open('hor.wav','wb')
wavwrite(f,sr,array(amp*sigout/max(sigout), dtype='int16'))
f.close()
