import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
from numpy.fft import fft, ifft
import scipy.signal as sig
from scipy.io.wavfile import read as wavread
from scipy.io.wavfile import write as wavwrite

import projet

f = open('phrase.wav','rb')
(fs, x) = wavread(f)
amp = max(x)
f.close()

# define factor
factor = 1.5
flen = 0.1
fhop = 0.01

# naive method
f = open('slow1.wav', 'wb')
wavwrite(f, int(fs/factor), x)
f.close()

# stft
fsize = int(fs*flen)
T = int(fs*fhop)
X = projet.stft(x, fsize, T)

# test istft
x2 = projet.istft(X, T)
f = open('phrase2.wav', 'wb')
wavwrite(f, fs, x2/max(x2))
f.close()

# not so naive method but still a bit
# test istft
x3 = projet.istft(X, int(T*factor))
f = open('slow2.wav', 'wb')
wavwrite(f, fs, np.array(x3/max(x3)))
f.close()

# with h_rephase
X2 = projet.h_rephase(X, T, int(T*factor))
x4 = projet.istft(X2, int(T*factor))
f = open('slow3.wav', 'wb')
wavwrite(f, fs, np.array(x4/max(x4)))
f.close()

# with h_rephase
X3 = projet.h_rephase_acc(X, T, int(T*factor), 10)
x5 = projet.istft_acc(X3, int(T*factor),10)
f = open('slow4.wav', 'wb')
wavwrite(f, fs, np.array(x5/max(x5)))
f.close()

# higher but same length
f = open('high.wav', 'wb')
wavwrite(f, int(fs*factor), x4/max(x4))
f.close()
